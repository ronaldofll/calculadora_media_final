package com.ronaldo.calcmf.vo;

/**
 * Created by ronaldo on 18/06/16.
 */
public class Nota {

    private Double nota;
    private Integer peso;

    public Double getNota() {
        return nota;
    }

    public void setNota(Double nota) {
        this.nota = nota;
    }

    public Integer getPeso() {
        return peso;
    }

    public void setPeso(Integer peso) {
        this.peso = peso;
    }
}
