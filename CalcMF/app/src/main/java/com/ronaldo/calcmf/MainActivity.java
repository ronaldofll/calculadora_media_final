package com.ronaldo.calcmf;

import android.content.Context;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.TextView;

import com.ronaldo.calcmf.components.Notas;

import java.text.DecimalFormat;
import java.text.NumberFormat;

public class MainActivity extends AppCompatActivity{

    public static final int MSG_CALCULATE_FINAL_RESULT = 1000;

    public static final String KEY_FINAL_RESULT = "finalResult";

    ViewGroup viewGroup;

    Notas notas;
    TextView resultadoTextView;
    TextView mediaSemestralTextView;
    NumberFormat nf;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main_activity);

        nf = new DecimalFormat("#,##0.00;-#,##0.00");

        viewGroup = (ViewGroup) getCurrentFocus();

        RadioButton opcMediaRadioButton = (RadioButton) findViewById(R.id.media_opc);
        final RadioButton opcNotasRadioButton = (RadioButton) findViewById(R.id.notas_opc);

        final Button addNotaButton = (Button) findViewById(R.id.addNota);
        final Button removeNotaButton = (Button) findViewById(R.id.removeNota);
        final Button calcularButton = (Button) findViewById(R.id.calcularButton);

        final EditText mediaSemestralEditText = (EditText) findViewById(R.id.media_semestral);
        mediaSemestralTextView = (TextView) findViewById(R.id.mediaTextView);
        notas = (Notas) findViewById(R.id.notas);

        resultadoTextView = (TextView) findViewById(R.id.resultadoTextView);

        opcNotasRadioButton.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    mediaSemestralTextView.setText(nf.format(0d));
                    resultadoTextView.setText(nf.format(0d));
                    mediaSemestralEditText.setVisibility(View.GONE);
                    notas.setVisibility(View.VISIBLE);
                    addNotaButton.setVisibility(View.VISIBLE);
                    removeNotaButton.setVisibility(View.VISIBLE);
                    calcularButton.setVisibility(View.VISIBLE);
                    notas.novaNota();
                }
            }
        });

        opcMediaRadioButton.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if(isChecked){
                    mediaSemestralEditText.setVisibility(View.VISIBLE);
                    notas.setVisibility(View.GONE);
                    addNotaButton.setVisibility(View.GONE);
                    removeNotaButton.setVisibility(View.GONE);
                    calcularButton.setVisibility(View.GONE);
                    notas.removeAllViews();
                }
            }
        });

        addNotaButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                notas.novaNota();
                removeNotaButton.setVisibility(View.VISIBLE);
            }
        });

        removeNotaButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                notas.removerNota();
                if (notas.getChildCount() == 0) {
                    removeNotaButton.setVisibility(View.GONE);
                }
            }
        });

        calcularButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                calculeMediaFinal(notas.getMedia());
                InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(mediaSemestralEditText.getWindowToken(), 0);
            }
        });

        mediaSemestralEditText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {}

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (!mediaSemestralEditText.getText().toString().isEmpty()) {
                    String mediaSemestral = mediaSemestralEditText.getText().toString().trim();
                    mediaSemestral = mediaSemestral.replace(",", ".");
                    try {
                        calculeMediaFinal(Double.parseDouble(mediaSemestral));
                    }catch (NumberFormatException e){
                        mediaSemestralTextView.setText(nf.format(0d));
                        resultadoTextView.setText(nf.format(0d));
                    }
                }else{
                    mediaSemestralTextView.setText(nf.format(0d));
                    resultadoTextView.setText(nf.format(0d));
                }
            }

            @Override
            public void afterTextChanged(Editable s) {
                if(s.toString() != null && !s.toString().isEmpty()) {
                    if (s.toString().matches(getString(R.string.regex_rate))) {
                        InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                        imm.hideSoftInputFromWindow(mediaSemestralEditText.getWindowToken(), 0);
                    }
                }
            }
        });
    }


    public void calculeMediaFinal(Double media) {
        if (media != null && media >= 0) {
            mediaSemestralTextView.setText(nf.format(media));
            if (media < 7) {
                double finalMedia = ((-media * 6) + 50) / 4;
                if (finalMedia > 10) {
                    resultadoTextView.setText(getString(R.string.msg_rate_more_than_necessary));
                } else {
                    resultadoTextView.setText(nf.format(finalMedia));
                }
            } else {
                resultadoTextView.setText(getString(R.string.msg_rate_less_than_required));
            }
        }
    }
}
