package com.ronaldo.calcmf.components;

import android.content.Context;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.LinearLayout;

import com.ronaldo.calcmf.R;

import java.util.ArrayList;
import java.util.List;
import java.util.zip.Inflater;

/**
 * Created by ronaldo on 18/06/16.
 */
public class Notas extends LinearLayout{

    public Notas(Context context) {
        super(context);
    }

    public Notas(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public Notas(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    public double getMedia(){
        double soma = 0;
        double qtd = 0;
        for(int i = 0; i<this.getChildCount();i++){
            View view = getChildAt(i);
            final EditText notaEditText = (EditText) view.findViewById(R.id.nota);
            EditText pesoEditText = (EditText) view.findViewById(R.id.peso);

            String notaString = notaEditText.getText().toString().trim();
            notaString = notaString.replace(",",".");
            String pesoString = pesoEditText.getText().toString().trim();
            pesoString = pesoString.replace(",",".");

            Double nota = 0.0;
            Double peso = 0.0;

            if(!notaString.isEmpty()) {
                 nota = Double.parseDouble(notaString);
            }
            if(!pesoString.isEmpty()){
                peso = Double.parseDouble(pesoString);
            }

            soma += nota*peso;
            qtd += peso;

            notaEditText.addTextChangedListener(new TextWatcher() {
                @Override
                public void beforeTextChanged(CharSequence s, int start, int count, int after) {

                }

                @Override
                public void onTextChanged(CharSequence s, int start, int before, int count) {

                }

                @Override
                public void afterTextChanged(Editable s) {
                    if(s.toString() != null && !s.toString().isEmpty()) {
                        if (s.toString().matches(getContext().getString(R.string.regex_rate))) {
                            notaEditText.clearFocus();
                        }
                    }
                }
            });
        }
        return soma/qtd;
    }

    public void novaNota(){
        LayoutInflater li = (LayoutInflater) getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View novaNota = li.inflate(R.layout.notas_layout, null, true);

        if(novaNota == this.getChildAt(0)){
            this.addView(novaNota);
        }
        this.addView(novaNota);
    }

    public void removerNota(){
        if(this.getChildCount()>0){
            this.removeViewAt(this.getChildCount()-1);
        }
    }
}
